package org.vhom.tasks.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.vhom.tasks.model.Task;
import org.vhom.tasks.model.User;
import org.vhom.tasks.repositories.TaskRepository;
import org.vhom.tasks.services.JwtUserDetailsService;
import org.vhom.tasks.services.TaskService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@RestController
public class TaskController {

    @Autowired
    TaskService taskService;

    @Autowired
    JwtUserDetailsService userService;

    public Task optionalTask(Long id, User u) {
        Optional<Task> optTask =  taskService.find(id);
        if(optTask.isPresent()){
            Task t = optTask.get();
            if (u.getId() == t.getUser().getId()) {
                return t;
            } else {
                throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Task forbidden");
            }
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Task not found");
        }
    }

    @RequestMapping(path="/tasks",method = RequestMethod.GET)
    public List<Task> getTasks(Authentication auth) {
        User u = userService.findUserFromAuth(auth);
        Set<Task> tasks = u.getTasks();
        System.out.println("Tasks: " + tasks.size());
        return new ArrayList<>(tasks);
    }

    @RequestMapping(path="/tasks/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getTask(Authentication auth, @PathVariable(value="id") Long id){
        User u = userService.findUserFromAuth(auth);
        return ResponseEntity.ok(optionalTask(id, u));
    }

    @RequestMapping(path="/tasks",method = RequestMethod.POST)
    public Task createNewTask(Authentication auth, @RequestBody Task t) {
        t.setUser(userService.findUserFromAuth(auth));
        return taskService.save(t);
    }

    @RequestMapping(path="/tasks/{id}", method = RequestMethod.POST)
    public Task updateTask(Authentication auth, @PathVariable(value="id") Long id, @RequestBody Task t) {
        User u = userService.findUserFromAuth(auth);
        Task origTask = optionalTask(id, u);
        origTask.setTitle(t.getTitle());
        origTask.setContent(t.getContent());
        origTask.setUser(u);
        return taskService.save(origTask);
    }

    @RequestMapping(path="/tasks/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteTask(Authentication auth, @PathVariable(value = "id") Long id) {
        Task t = optionalTask(id, userService.findUserFromAuth(auth));
        taskService.delete(t);
        return ResponseEntity.noContent().build();
    }
}
