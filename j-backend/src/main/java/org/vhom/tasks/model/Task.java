package org.vhom.tasks.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;

@Entity
@Table(name="tasks")
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column
    private String title;

    @Column
    private String content;

    @Column
    private Boolean completed;

    @ManyToOne
    @JoinColumn(name="user_id",nullable=false)
    @JsonBackReference
    private User user;

    Task() {}

    Task(String title, String content, User user){
        setContent(content);
        setTitle(title);
        setUser(user);
    }

    Task(long id, String title, String content, User user){
        setId(id);
        setContent(content);
        setTitle(title);
        setUser(user);
    }

    public long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }

    public String getTitle() {
        return title;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }
}
