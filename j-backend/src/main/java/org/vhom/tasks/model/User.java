package org.vhom.tasks.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name="users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private long id;

    @Column(name = "username", unique = true)
    @JsonProperty("username")
    private String username;

    @Column(name="first_name")
    @JsonProperty("first_name")
    private String firstName;

    @Column(name="last_name")
    @JsonProperty("last_name")
    private String lastName;

    @Column(name="email",unique = true)
    private String email;

    @Column
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    @OneToMany(mappedBy = "user")
    @JsonManagedReference
    private Set<Task> tasks;

    User(){}

    User(String fName, String lName, String email) {
        setFirstName(fName);
        setLastName(lName);
        setEmail(email);
    }

    User(Long id, String fName, String lName, String email) {
        setId(id);
        setFirstName(fName);
        setLastName(lName);
        setEmail(email);
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLastName() {
        return lastName;
    }

    @JsonIgnore
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String print() {
        return String.format("{'first_name': %s, 'last_name': %s, 'id': %d, 'email': %s, 'password': %s, 'username': %s}",
                getFirstName(), getLastName(), getId(),getEmail(),getPassword(),getUsername());
    }

    public Set<Task> getTasks() {
        return tasks;
    }
}
