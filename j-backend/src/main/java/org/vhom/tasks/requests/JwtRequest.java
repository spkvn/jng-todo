package org.vhom.tasks.requests;

import java.io.Serializable;

public class JwtRequest implements Serializable {

    private String email;
    private String username;
    private String password;

    //need default constructor for JSON Parsing
    public JwtRequest() {    }

    public JwtRequest(String email, String username, String password) {
        this.setEmail(email);
        this.setUsername(username);
        this.setPassword(password);
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }
}
