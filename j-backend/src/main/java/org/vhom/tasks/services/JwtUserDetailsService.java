package org.vhom.tasks.services;

import java.util.ArrayList;

import net.bytebuddy.asm.Advice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.vhom.tasks.repositories.UserRepository;

@Service
public class JwtUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private PasswordEncoder bcryptEncoder;

    public org.vhom.tasks.model.User findUserFromAuth(Authentication auth) {
        UserDetails ud = (UserDetails) auth.getPrincipal();
        org.vhom.tasks.model.User u = findByUsername(ud.getUsername());
        return u;
    }

    public org.vhom.tasks.model.User findByUsername(String username) {
        org.vhom.tasks.model.User u = userRepo.findByUsername(username);
        if (u == null) {
            throw new UsernameNotFoundException("User not found with username of :" + username);
        }
        return u;
    }

    public org.vhom.tasks.model.User findByEmail(String username) {
        org.vhom.tasks.model.User u = userRepo.findByEmail(username);
        if (u == null) {
            throw new UsernameNotFoundException("User not found with username of :" + username);
        }
        return u;
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        org.vhom.tasks.model.User u = findByUsername(username);
        return new User(u.getUsername(), u.getPassword(), new ArrayList<>());
    }

    public UserDetails loadUserByEmail(String email) throws UsernameNotFoundException {
        org.vhom.tasks.model.User u = findByEmail(email);
        return new User(u.getEmail(), u.getPassword(), new ArrayList<>());
    }

    public org.vhom.tasks.model.User save(org.vhom.tasks.model.User user) {
        System.out.println("============================================");
        System.out.println(user.print());
        user.setPassword(bcryptEncoder.encode(user.getPassword()));
        return userRepo.save(user);
    }
}