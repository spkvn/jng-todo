package org.vhom.tasks.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.vhom.tasks.model.Task;
import org.vhom.tasks.repositories.TaskRepository;

import java.util.List;
import java.util.Optional;

@Service
public class TaskService {

    @Autowired
    private TaskRepository taskRepo;

    public List<Task> list() {
        return taskRepo.findAll();
    }

    public Optional<Task> find(Long id) {
        return taskRepo.findById(id);
    }

    public Task save(Task t) {
        return taskRepo.save(t);
    }

    public void delete(Task t) {
        taskRepo.delete(t);
    }
}
