angular.module("tasks").component("login", {
    templateUrl: "components/login/login.html",
    controller: function($http, TokenService, $state){
        this.username = "";
        this.password = "";

        this.formComplete = function(){
            return this.username !== "" && this.password !== "";
        };

        this.generateFormData = function(){
            return {
                "username": this.username,
                "password": this.password
            }
        };

        this.submit = function(){
            if(this.formComplete()){
                $http.post("http://localhost:8080/authenticate", this.generateFormData())
                    .then(function(response) {
                        TokenService.put(response.data.token);
                        $state.go("tasks");
                    })
                    .catch(function(error){
                        console.error(error);
                    })
            }
        };
    }
});