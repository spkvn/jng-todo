angular.module("tasks").component("register",{
    templateUrl:"components/register/register.html",
    controller: function($http, TokenService) {
        this.properties = [
            {key: "first_name", label: "First Name", input: "", type: "text"},
            {key: "last_name", label: "Last Name", input: "", type: "text"},
            {key: "email", label: "Email", input: "", type: "email"},
            {key: "username", label: "Username", input: "", type: "text"},
            {key: "password", label: "Password", input: "", type: "password"},
            {key: "password_confirmation", label: "Password Confirmation", input: "", type: "password"}
        ];

        this.getPropertyWithKey = function(key){
            return this.properties.filter(function(prop) {
                return prop.key === key;
            })[0];
        };

        this.passwordsMatch = function(){
            return this.getPropertyWithKey('password').input === this.getPropertyWithKey("password_confirmation").input;
        };

        this.allPropertiesSet = function(){
            var allSet = true;
            this.properties.forEach(function(property){
                if(property.input === "" || property.input === null) {
                    allSet = false;
                }
            });
            return allSet;
        };

        this.generatePostProps = function(){
            return {
                "username": this.getPropertyWithKey("username").input,
                "email": this.getPropertyWithKey("email").input,
                "first_name": this.getPropertyWithKey("first_name").input,
                "last_name": this.getPropertyWithKey("last_name").input,
                "password": this.getPropertyWithKey("password").input
            };
        };

        this.submit = function(){
            var validRequest = this.passwordsMatch() && this.allPropertiesSet();
            if(validRequest) {
                var data = this.generatePostProps();
                $http.post("http://localhost:8080/register", this.generatePostProps())
                    .then(function(response){
                        console.log(response);
                        TokenService.put(response.data.token)
                    })
                    .catch(function(error){
                        console.error(error);
                    });
            }
        };
    }
});