angular.module("tasks").component("tasksAll", {
    templateUrl: "components/tasks/all/all.html",
    bindings: {
        taskList: "<" // one way binding ... ?
    },
    controller: function(TaskService, TokenService, $state) {
        this.logout = function(){
            TokenService.put(null);
            $state.go("anon");
        }
    }
});