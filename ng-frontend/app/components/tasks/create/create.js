angular.module("tasks").component("tasksCreate",{
    templateUrl: "components/tasks/create/create.html",
    controller: function(TaskService, $state) {
        this.saving = false;
        this.task = {
            title: "",
            content: "",
            completed: false
        };

        this.save = function() {
            this.saving = true;
            TaskService.post(this.task).then(function(){
                this.saving = false;
                $state.transitionTo("tasks", {},{reload:true});
            }).catch(function(){
                this.saving = false;
            });
        }
    }
});