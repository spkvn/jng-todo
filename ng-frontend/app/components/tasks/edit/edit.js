angular.module("tasks").component("tasksEdit",{
    templateUrl: "components/tasks/edit/edit.html",
    bindings: {
        task: "<"
    },
    controller: function(TaskService, $state){
        this.loading = false;
        this.deleting = false;
        this.saveTask = function(){
            this.loading = true;
            TaskService.update(this.task.id, this.task).then(function(){
                this.loading = false;
                $state.transitionTo("tasks",{},{reload:true});
            }).catch(function(){
                this.loading = false;
            })
        };

        this.deleteTask = function(){
            this.deleting = true;
            TaskService.delete(this.task.id).then(function(){
                this.deleting = false;
                $state.transitionTo("tasks",{},{reload:true});
            }).catch(function(){
                this.deleting = false;
            })
        }
    }
});