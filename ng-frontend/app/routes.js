angular.module("tasks").config(function($stateProvider, $urlRouterProvider){
    $urlRouterProvider.otherwise("/home");
    var states = [
        {
            name: "anon",
            url: "/home",
            component: "helloWorld"
        },{
            name: "anon.login",
            url: "/login",
            component: "login"
        },{
            name: "anon.register",
            url: "/register",
            component: "register"
        },{
            name: "tasks",
            cache: false,
            url: "/tasks",
            component: "tasksAll",
            resolve: {
                taskList: function(TaskService){
                    return TaskService.list();
                }
            }
        },{
            name: "tasks.create",
            url : "/create",
            component: "tasksCreate"
        },{
            name: "tasks.edit",
            url: "/:taskId",
            component: "tasksEdit",
            resolve: {
                task: function($transition$, taskList){
                    var task = taskList.data.find(function(task){
                        return parseInt(task.id) === parseInt($transition$.params().taskId);
                    });
                    return {
                        id: task.id,
                        title: task.title,
                        content: task.content,
                        completed: task.completed
                    };
                }
            }
        }
    ];

    states.forEach(function(state){
        $stateProvider.state(state);
    });

});

