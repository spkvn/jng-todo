angular.module("tasks").factory("TaskService", function(TokenService, $http){
    var TaskService = {
        BASE_URL: "http://localhost:8080"
    };

    TaskService.list = function(){
        var url = TaskService.BASE_URL + "/tasks";
        var request = TokenService.requestWithToken("GET", url);
        return $http(request);
    };

    TaskService.get = function(id){
        var url = TaskService.BASE_URL + "/tasks/" + id;
        var req = TokenService.requestWithToken("GET", url);
        return $http(req);
    };

    TaskService.post = function(task){
        var url = TaskService.BASE_URL + "/tasks";
        var req = TokenService.requestWithToken("POST", url, task);
        return $http(req);
    };

    TaskService.update = function(id, task){
        var url = TaskService.BASE_URL + "/tasks/" + id;
        var req = TokenService.requestWithToken("POST", url, task);
        return $http(req);
    };

    TaskService.delete = function(id) {
        var url = TaskService.BASE_URL + "/tasks/" + id
        var req = TokenService.requestWithToken("DELETE", url);
        return $http(req);
    };


    return TaskService;
});