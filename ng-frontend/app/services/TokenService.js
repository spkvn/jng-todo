angular.module("tasks").factory("TokenService",["$cookies", function($cookies){
    var TokenService = {};
    
    TokenService.put = function(token){
        $cookies.put("token",token);
    };
    TokenService.get = function(){
        return $cookies.get("token");
    };

    TokenService.requestWithToken = function(verb, url, data){
        return {
            method: verb,
            url: url,
            data: data,
            headers: {
                "Authorization": "Bearer " + TokenService.get(),
            }
        };
    };
    
    return TokenService;
}]);